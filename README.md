Express Delivery
=========

This is a full featured module template for Dolibarr.
It's a tool for module developpers to kickstart their project.

If you're not a module developper you have no use for this.

Licence
-------
GPLv3 or (at your option) any later version.

See COPYING for more information.

INSTALL
-------
Check in english :
[wiki pages](http://wiki.dolibarr.org/index.php/FAQ_ModuleCustomDirectory)

A lire  en français :
[pages wiki](http://wiki.dolibarr.org/index.php/FAQ_Repertoire_Custom_Module_Externe)

- From your browser, log in as administrator dolibarr
  and left click on the "configuration" menu.
  Then click on the submenu "module".
  On the screen that appears and you whouls be able to see the new module (check all tabs, ban be in other than first one)
  The status menu should then proceed to "Enable" menu "Management
  training "should appear at the top of the application.
- Check the security right (Users->permitions) to be sure that right are correctly set for user or users group

PROJECT PAGES:
--------------
[https://doliforge.org/projects/expressdelivery/](https://doliforge.org/projects/expressdelivery/)

Other Licences
--------------

Uses [Michel Fortin's PHP Markdown](http://michelf.ca/projets/php-markdown/) Licensed under BSD to display this README in the module's about page.
